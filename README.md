
#include <graphics.h>
#include <conio.h>
#include <math.h>
#include <stdio.h>
#include <time.h>
#pragma comment(lib,"Winmm.lib")
#define High 600     // 游戏时画面尺寸
#define Width 800
#define Num 40


//全局变量

IMAGE  img_bk1, img_bk2, img_help;//定义开始背景，帮助，游戏背景图片

IMAGE   img_right, img_right_mask,img_left,img_left_mask;

IMAGE    img_be_hit, img_be_hit_mask,
        img_big, img_big_mask,
        img_small, img_small_mask;

IMAGE   img_left_hit1, img_left_hit1_mask,
        img_left_hit2, img_left_hit2_mask ,
        img_left_jump ,img_left_jump_mask ,
        img_left_jump_hit ,img_left_jump_hit_mask ,
        img_left_ray ,img_left_ray_mask ,
        img_left_squat ,img_left_squat_mask;

IMAGE   img_right_hit1, img_right_hit1_mask,
        img_right_hit2, img_right_hit2_mask ,
        img_right_jump ,img_right_jump_mask ,
        img_right_jump_hit ,img_right_jump_hit_mask ,
        img_right_ray ,img_right_ray_mask ,
        img_right_squat ,img_right_squat_mask;

IMAGE  img_zombie1,img_zombie1_mask,img_zombie2,img_zombie2_mask,
       img_zombie3,img_zombie3_mask,img_zombie4,img_zombie4_mask;

IMAGE img_coming ,img_coming_mask ,img_failure, img_failure_mask ,
       img_inging ,img_inging_mask ,img_prepare ,img_prepare_mask ,
       img_win ,img_win_mask ,img_sun,img_sun_mask;

IMAGE img_ing0, img_ing0_mask,  img_ing1,  img_ing1_mask,  img_ing2,
      img_ing2_mask,  img_ing3,  img_ing3_mask,  img_ing4,  img_ing4_mask,
      img_ing5,  img_ing5_mask,  img_ing6,  img_ing6_mask,  img_ing7,  
      img_ing7_mask,  img_ing8,  img_ing8_mask,  img_ing9,  img_ing9_mask,
      img_ing10,  img_ing10_mask;


IMAGE img_0,  img_0_mask,  img_1,  img_1_mask,  img_2,  img_2_mask,
      img_3,  img_3_mask,  img_4,  img_4_mask,  img_5,  img_5_mask,  
      img_6,  img_6_mask,  img_7,  img_7_mask,  img_8,  img_8_mask,
      img_9,  img_9_mask,  img_sunwindow,  img_sunwindow_mask ;

IMAGE img_manblood0,   img_manblood0_mask,   img_manblood1,
       img_manblood1_mask,   img_manblood2,   img_manblood2_mask,
       img_manblood3,   img_manblood3_mask,   img_manblood4,
       img_manblood4_mask,   img_manblood5,   img_manblood5_mask,
       img_manblood6,   img_manblood6_mask,   img_manblood7,  
       img_manblood7_mask,   img_manblood8,   img_manblood8_mask,
       img_manblood9,   img_manblood9_mask,img_manblood10,
       img_manblood10_mask,img_zombiehead,img_zombiehead_mask;


                    
int        show_help=0;     //是否显示帮助画面
int        show_game=0;      //是否显示帮助画面
int        show_start=1;     //是否显示开始画面

int man_x,man_y;  //奥特曼的坐标


int right =1,left=0;  //奥特曼方向

int man_i=0; //奥特曼行走时的图片格数

int is_hit1=0,is_hit2=0;//是否击打

int  have_hit=0;   //是否已经出拳

int is_playhelp=0;    //是否播放帮助音乐
int is_playstart=0;   //是否播放开始音乐
int is_playgame=0;    //是否播放游戏时音乐

int had_playhelp=0;    //是否已经播放帮助音乐
int had_playstart=0;   //是否已经播放开始音乐
int had_playgame=0;    //是否已经播放游戏时音乐
int had_playbehit=0;   //是否已经播放被击打的音乐
int had_playprepare=0; //是否已经播放准备音乐
int had_playlast=0;    //是否已经播放最后一波音乐

int is_playhit1music=0;//是否播放击打音效1
int is_playhit2music=0;//是否播放击打音效2

int have_playhit1=0;//是否已经播放击打音效1
int have_playhit2=0;//是否已经播放击打音效2

int  zombie_startx[5][Num];//僵尸开始的位置

int  zombie_x[5][Num],zombie_y[5][Num];//僵尸的位置

int zombie_j[5][Num];    //僵尸的图片位置

int is_zombie_dead[5][Num]; //僵尸是否死亡  1是死亡  0是未死亡

int zombieblood[5][Num];//僵尸血量   不同僵尸血量不同

int is_show_zombie[5][Num];//是否显示僵尸  1显示  0不显示

int  zombie_type[5][Num];//僵尸的类型,1代表普通僵尸，2代表路障僵尸，3代表铁通僵尸，4代表铁门僵尸

int manblood;//奥特曼的血量

int be_hit; //奥特曼是否被击打

int behit1,behit2;//记录奥特曼被打

int i;//循环用到的变量
int j;

int bk_i,bk_j;//背景的位置

int  starttime;//开始时间
int  nowtime;  //现在时间
int  nowtime1[5][Num];  //记录僵尸的暂停时间
int  nowtime2[5][Num];  
int     hadtime1[5][Num];//是否记录时间
int     hadtime2[5][Num];//是否记录时间
int  gonetime;//已经进行的时间
int  behittime1,behittime2;//记录及被击打的时间
int  hadhittime1,hadhittime2;//是否记录被击打的时间
int  timenum;

int zombie_stop[5][Num];//僵尸是否移动

int man_hit;  //奥特曼是否在击打僵尸

int is_man_dead;//奥特曼是否死亡

int sunNum;//阳光的数量

int show_sun[30];//是否显示阳光

int sun_x[30],sun_y[30];//阳光的坐标

int showray;//是否发射激光

int raytime1,raytime2;//记录激光的时间

int zombiehead_x;

int win ,failure;//胜利和失败；
int win_i;//记录胜利的变量
int showprepare,showlast;//是否显示准备和最后一波

int stop=0;//是否停止

//播放开始音乐
void playstartmusic()
{
     mciSendString("close startmusic", NULL, 0, NULL); //关闭开始音乐
     mciSendString("close helpmusic", NULL, 0, NULL); //关闭帮助音乐
    mciSendString("open .\\start.mp3 alias startmusic",NULL,0,NULL);//背景音乐
    mciSendString("play startmusic repeat",NULL,0,NULL);//循环播放
}

//播放帮助音乐
void playhelpmusic()
{
    mciSendString("close helpmusic", NULL, 0, NULL); //关闭帮助音乐
     mciSendString("close startmusic", NULL, 0, NULL); //关闭开始音乐
    mciSendString("open .\\help.mp3 alias helpmusic",NULL,0,NULL);//背景音乐
    mciSendString("play helpmusic repeat",NULL,0,NULL);//循环播放
}

//播放游戏音乐
void playgamemusic()
{
    mciSendString("close helpmusic", NULL, 0, NULL); //关闭帮助音乐
    mciSendString("close startmusic", NULL, 0, NULL); //关闭开始音乐
    mciSendString("open .\\game.mp3 alias gamemusic",NULL,0,NULL);//背景音乐
    mciSendString("play gamemusic repeat",NULL,0,NULL);//循环播放
}

//播放击打音效
void  playhit1music()
{

    mciSendString("open .\\hit1.wav alias hit1music",NULL,0,NULL);
    mciSendString("play hit1music repeat",NULL,0,NULL);
}
void  playhit2music()
{
    mciSendString("open .\\hit2.mp3 alias hit2music",NULL,0,NULL);
    mciSendString("play hit2music repeat",NULL,0,NULL);

}

void playbehitmusic()
{
    mciSendString("close behitmusic",NULL,0,NULL);//关闭先前一次的音乐
    mciSendString("open .\\behit.mp3 alias behitmusic",NULL,0,NULL);
    mciSendString("play behitmusic ",NULL,0,NULL);

}

void startup()
{
    initgraph(Width, High);//游戏时画面大小


    loadimage(&img_bk1,".\\bk1.jpg");   //引入开始背景图片
    loadimage(&img_bk2,".\\bk2.jpg");   //引入游戏背景图片
    loadimage(&img_help,".\\help.jpg");   //引入帮助图片

    
    loadimage(&img_be_hit,".\\be_hit.png");
    loadimage(&img_be_hit_mask,".\\be_hit_mask.png");
    
    loadimage(&img_left,".\\left.png");
    loadimage(&img_left_mask,".\\left_mask.png");
    loadimage(&img_left_hit1,".\\left_hit1.png");
    loadimage(&img_left_hit1_mask,".\\left_hit1_mask.png");
    loadimage(&img_left_hit2,".\\left_hit2.png");
    loadimage(&img_left_hit2_mask,".\\left_hit2_mask.png");
    loadimage(&img_left_jump,".\\left_left_jump.png");
    loadimage(&img_left_jump_mask,".\\left_jump_mask.png");
    loadimage(&img_left_jump_hit,".\\left_jump_hit.png");
    loadimage(&img_left_jump_hit_mask,".\\left_jump_hit_mask.png");
    loadimage(&img_left_ray,".\\left_ray.png");
    loadimage(&img_left_ray_mask,".\\left_ray_mask.png");
    loadimage(&img_left_squat,".\\left_squat.png");
    loadimage(&img_left_squat_mask,".\\left_squat_mask.png");
    
    loadimage(&img_right,".\\right.png");
    loadimage(&img_right_mask,".\\right_mask.png");
    loadimage(&img_right_hit1,".\\right_hit1.png");
    loadimage(&img_right_hit1_mask,".\\right_hit1_mask.png");
    loadimage(&img_right_hit2,".\\right_hit2.png");
    loadimage(&img_right_hit2_mask,".\\right_hit2_mask.png");
    loadimage(&img_right_jump,".\\right_right_jump.png");
    loadimage(&img_right_jump_mask,".\\right_jump_mask.png");
    loadimage(&img_right_jump_hit,".\\right_jump_hit.png");
    loadimage(&img_right_jump_hit_mask,".\\right_jump_hit_mask.png");
    loadimage(&img_right_ray,".\\right_ray.png");
    loadimage(&img_right_ray_mask,".\\right_ray_mask.png");
    loadimage(&img_right_squat,".\\right_squat.png");
    loadimage(&img_right_squat_mask,".\\right_squat_mask.png");

    loadimage(&img_zombie1,".\\zombie1.png");
    loadimage(&img_zombie1_mask,".\\zombie1_mask.png");
    loadimage(&img_zombie2,".\\zombie2.png");
    loadimage(&img_zombie2_mask,".\\zombie2_mask.png");
    loadimage(&img_zombie3,".\\zombie3.png");
    loadimage(&img_zombie3_mask,".\\zombie3_mask.png");
    loadimage(&img_zombie4,".\\zombie4.png");
    loadimage(&img_zombie4_mask,".\\zombie4_mask.png");

    
    loadimage(&img_coming,".\\coming.png");
    loadimage(&img_coming_mask,".\\coming_mask.png");
    loadimage(&img_failure,".\\failure.png");
    loadimage(&img_failure_mask,".\\failure_mask.png");
    loadimage(&img_inging,".\\inging.png");
    loadimage(&img_inging_mask,".\\inging_mask.png");
    loadimage(&img_prepare,".\\prepare.png");
    loadimage(&img_prepare_mask,".\\prepare_mask.png");
    loadimage(&img_win,".\\win.png");
    loadimage(&img_win_mask,".\\win_mask.png");
    loadimage(&img_zombiehead,".\\zombiehead.png");
    loadimage(&img_zombiehead_mask,".\\zombiehead_mask.png");
    
    loadimage(&img_ing0,".\\ing0.png");
    loadimage(&img_ing0_mask,".\\ing0_mask.png");
    loadimage(&img_ing1,".\\ing1.png");
    loadimage(&img_ing1_mask,".\\ing1_mask.png");
    loadimage(&img_ing2,".\\ing2.png");
    loadimage(&img_ing2_mask,".\\ing2_mask.png");
    loadimage(&img_ing3,".\\ing3.png");
    loadimage(&img_ing3_mask,".\\ing3_mask.png");
    loadimage(&img_ing4,".\\ing4.png");
    loadimage(&img_ing4_mask,".\\ing4_mask.png");
    loadimage(&img_ing5,".\\ing5.png");
    loadimage(&img_ing5_mask,".\\ing5_mask.png");
    loadimage(&img_ing6,".\\ing6.png");
    loadimage(&img_ing6_mask,".\\ing6_mask.png");
    loadimage(&img_ing7,".\\ing7.png");
    loadimage(&img_ing7_mask,".\\ing7_mask.png");
    loadimage(&img_ing8,".\\ing8.png");
    loadimage(&img_ing8_mask,".\\ing8_mask.png");
    loadimage(&img_ing9,".\\ing9.png");
    loadimage(&img_ing9_mask,".\\ing9_mask.png");
    loadimage(&img_ing10,".\\ing10.png");
    loadimage(&img_ing10_mask,".\\ing10_mask.png");
    
    loadimage(&img_0,".\\0.png");
    loadimage(&img_0_mask,".\\0_mask.png");
    loadimage(&img_1,".\\1.png");
    loadimage(&img_1_mask,".\\1_mask.png");
    loadimage(&img_2,".\\2.png");
    loadimage(&img_2_mask,".\\2_mask.png");
    loadimage(&img_3,".\\3.png");
    loadimage(&img_3_mask,".\\3_mask.png");
    loadimage(&img_4,".\\4.png");
    loadimage(&img_4_mask,".\\4_mask.png");
    loadimage(&img_5,".\\5.png");
    loadimage(&img_5_mask,".\\5_mask.png");
    loadimage(&img_6,".\\6.png");
    loadimage(&img_6_mask,".\\6_mask.png");
    loadimage(&img_7,".\\7.png");
    loadimage(&img_7_mask,".\\7_mask.png");
    loadimage(&img_8,".\\8.png");
    loadimage(&img_8_mask,".\\8_mask.png");
    loadimage(&img_9,".\\9.png");
    loadimage(&img_9_mask,".\\9_mask.png");
    loadimage(&img_sunwindow,".\\sunwindow.png");
    loadimage(&img_sunwindow_mask,".\\sunwindow_mask.png");
    loadimage(&img_sun,".\\sun.png");
    loadimage(&img_sun_mask,".\\sun_mask.png");

    loadimage(&img_manblood0,".\\manblood0.png");
    loadimage(&img_manblood0_mask,".\\manblood0_mask.png");
    loadimage(&img_manblood1,".\\manblood1.png");
    loadimage(&img_manblood1_mask,".\\manblood1_mask.png");
    loadimage(&img_manblood2,".\\manblood2.png");
    loadimage(&img_manblood2_mask,".\\manblood2_mask.png");
    loadimage(&img_manblood3,".\\manblood3.png");
    loadimage(&img_manblood3_mask,".\\manblood3_mask.png");
    loadimage(&img_manblood4,".\\manblood4.png");
    loadimage(&img_manblood4_mask,".\\manblood4_mask.png");
    loadimage(&img_manblood5,".\\manblood5.png");
    loadimage(&img_manblood5_mask,".\\manblood5_mask.png");
    loadimage(&img_manblood6,".\\manblood6.png");
    loadimage(&img_manblood6_mask,".\\manblood6_mask.png");
    loadimage(&img_manblood7,".\\manblood7.png");
    loadimage(&img_manblood7_mask,".\\manblood7_mask.png");
    loadimage(&img_manblood8,".\\manblood8.png");
    loadimage(&img_manblood8_mask,".\\manblood8_mask.png");
    loadimage(&img_manblood9,".\\manblood9.png");
    loadimage(&img_manblood9_mask,".\\manblood9_mask.png");
    loadimage(&img_manblood10,".\\manblood10.png");
    loadimage(&img_manblood10_mask,".\\manblood10_mask.png");


    man_x=0;
    man_y=200;

    manblood=20;//奥特曼的血量为十
    behit1=0;
    behit2=1;
    timenum=0;//记录时间的变量
//游戏画面的位置的初始值
    bk_i=0;
    bk_j=-1;

    hadhittime1=0;
    hadhittime2=0;

//奥特曼初始为未在攻击
    man_hit=0;

//奥特曼初始未死亡
    is_man_dead=0;
//阳光的数量初始为零
    sunNum=0;
//开始不发射激光
    showray=0;

//开始僵尸头的位置
    zombiehead_x=732;
//开始不显示失败不显示胜利不显示准备不显示最后一波
    win=0;
    failure=0;
    showprepare=0;
    showlast=0;
    win_i=0;

//僵尸的类型，1是路障，2是铁通，3是铁门，4是橄榄球
    //第1,2方格
    for (i=0;i<5;i++)
    {
        for (j=0;j<10;j++)
        {
            zombie_type[i][j]=1;
            

        }
    }
    //第3方格
    for (i=0;i<5;i++)
    {
        for (j=10;j<15;j++)
        {
            zombie_type[i][j]=1+rand()%2;
            
        }
    }
    //第4方格
    for (i=0;i<5;i++)
    {
        for (j=15;j<20;j++)
        {
            zombie_type[i][j]=1+rand()%3;
            
        }
    }
    //第5方格
    for (i=0;i<5;i++)
    {
        for (j=20;j<25;j++)
        {
            zombie_type[i][j]=2+rand()%2;
            
        }
    }
    //第6方格
    for (i=0;i<5;i++)
    {
        for (j=25;j<30;j++)
        {
            zombie_type[i][j]=2+rand()%3;
            
        }
    }
    //第7方格
    for (i=0;i<5;i++)
    {
        for (j=30;j<35;j++)
        {
            zombie_type[i][j]=3+rand()%2;
            
        }
    }
    //第8方格
    for (i=0;i<5;i++)
    {
        for (j=35;j<40;j++)
        {
            zombie_type[i][j]=4;
            
        }
    }


    //第1方格每行显示2个

    for (i=0;i<5;i++)
    {
        for (j=0;j<5;j++)
        {
            is_show_zombie[i][j]=rand()%5+1;
            
        }
    }
    //第2方格每行显示3个
    
    for (i=0;i<5;i++)
    {
        for (j=5;j<10;j++)
        {
            is_show_zombie[i][j]=rand()%5+2;
            
        }
    }
    //第3方格每行显示4个
    
    for (i=0;i<5;i++)
    {
        for (j=10;j<15;j++)
        {
            is_show_zombie[i][j]=rand()%5+2;
            
        }
    }

    //第4方格每行显示4个
    
    for (i=0;i<5;i++)
    {
        for (j=15;j<20;j++)
        {
            is_show_zombie[i][j]=rand()%5+3;
            
        }
    }
    //第5方格每行显示4个
    
    for (i=0;i<5;i++)
    {
        for (j=20;j<25;j++)
        {
            is_show_zombie[i][j]=rand()%5+3;
            
        }
    }
    //第6方格每行显示4个
    
    for (i=0;i<5;i++)
    {
        for (j=25;j<30;j++)
        {
            is_show_zombie[i][j]=rand()%5+4;
            
        }
    }
    //第78方格僵尸全显示
    for (i=0;i<5;i++)
    {
        for (j=30;j<40;j++)
        {
            is_show_zombie[i][j]=5;
            
        }
    }
//僵尸开始时为活，血量为相应值
    for (i=0;i<5;i++)
    {
        for (j=0;j<40;j++)
        {
            is_zombie_dead[i][j]=0;
            zombieblood[i][j]=zombie_type[i][j]+3;
            nowtime1[i][j]=0;
            nowtime2[i][j]=0;
            // 僵尸在移动
            zombie_stop[i][j]=0;
        }
    }

    //僵尸的横纵坐标

    for (i=0;i<5;i++)
    {
        for (j=0;j<40;j++)
        {
            if (j>=35)
            {    
                zombie_startx[i][j]=1200+j*100;
                zombie_x[i][j]=1200+j*100;
            }
            else
            {
                zombie_startx[i][j]=900+j*100;
                zombie_x[i][j]=900+j*100;
            }
            
            if (i==0)
                zombie_y[i][j]=60;
            if (i==1)
                zombie_y[i][j]=150;
            if (i==2)
                zombie_y[i][j]=240;
            if (i==3)
                zombie_y[i][j]=325;
            if (i==4)
                zombie_y[i][j]=415;
        
        }
    }
    
    for (i=0;i<30;i++)
    {
        show_sun[i]=0;
        sun_x[i]=100+rand()%500;
        sun_y[i]=80+rand()%300;
    }


    
    BeginBatchDraw();
}

void show()
{

    if(show_start)
    {
    
        putimage(0,0,&img_bk1);//显示开始画面
        if (is_playstart==0)
        {
            is_playstart=1;
            is_playhelp=0;
            is_playgame=0;
        }

    }

    if(show_help)
    {
        putimage(0,0,&img_help);   //显示帮助画面
        if (is_playhelp==0)
        {
            is_playstart=0;
            is_playhelp=1;
            is_playgame=0;
        }

    }

    if(show_game)
    {
        putimage(bk_i,0,&img_bk2);   //显示游戏画面
        if (is_playgame==0)
        {
            is_playstart=0;
            is_playhelp=0;
            is_playgame=1;
        }

            // 显示僵尸

        for (i=0;i<5;i++)
        {
            for (j=0;j<40;j++)
            {
                if (is_show_zombie[i][j]>4&&is_zombie_dead[i][j]==0)
                {
                    //显示僵尸类型1
                    if (zombie_type[i][j]==1)
                    {
                        putimage(zombie_x[i][j],zombie_y[i][j],80,120,&img_zombie1_mask,zombie_j[i][j]*80,0,NOTSRCERASE);
                        putimage(zombie_x[i][j],zombie_y[i][j],80,120,&img_zombie1,zombie_j[i][j]*80,0,SRCINVERT);
                    }
                    //显示僵尸类型2
                    if (zombie_type[i][j]==2)
                    {
                        putimage(zombie_x[i][j],zombie_y[i][j],120,120,&img_zombie2_mask,zombie_j[i][j]*120,0,NOTSRCERASE);
                        putimage(zombie_x[i][j],zombie_y[i][j],120,120,&img_zombie2,zombie_j[i][j]*120,0,SRCINVERT);
                    }
                    //显示僵尸类型3
                    if (zombie_type[i][j]==3)
                    {
                        putimage(zombie_x[i][j],zombie_y[i][j],120,120,&img_zombie3_mask,zombie_j[i][j]*120,0,NOTSRCERASE);
                        putimage(zombie_x[i][j],zombie_y[i][j],120,120,&img_zombie3,zombie_j[i][j]*120,0,SRCINVERT);
                    }
                    //显示僵尸类型4
                    if (zombie_type[i][j]==4)
                    {
                        putimage(zombie_x[i][j],zombie_y[i][j],125,125,&img_zombie4_mask,zombie_j[i][j]*125,0,NOTSRCERASE);
                        putimage(zombie_x[i][j],zombie_y[i][j],125,125,&img_zombie4,zombie_j[i][j]*125,0,SRCINVERT);
                    }

                }
            }
        }




        if(right)
        {
            
            if(is_hit1)
            {
                putimage(man_x,man_y,140,140,&img_right_hit1_mask,0,0,NOTSRCERASE);
                putimage(man_x,man_y,140,140,&img_right_hit1,0,0,SRCINVERT);
                
                is_playhit1music=1;
            
                have_hit=1;
            }
            else if(is_hit2)
            {
                putimage(man_x,man_y,140,140,&img_right_hit2_mask,0,0,NOTSRCERASE);
                putimage(man_x,man_y,140,140,&img_right_hit2,0,0,SRCINVERT);

                is_playhit2music=1;

                have_hit=1;            
                
            }
            else if (be_hit)
            {
        
                if(  (nowtime-starttime)  - 1000  *  ( (nowtime-starttime)/1000)  >500 )
                {
                    putimage(man_x,man_y,140,140,&img_be_hit_mask,0,0,NOTSRCERASE);
                    putimage(man_x,man_y,140,140,&img_be_hit,0,0,SRCINVERT);
                    behit1=1;
                }
                else
                {
                    putimage(man_x,man_y,140,140,&img_right_mask,man_i*140,0,NOTSRCERASE);
                    putimage(man_x,man_y,140,140,&img_right,man_i*140,0,SRCINVERT);    
                    behit1=0;
                    behit2=1;
                }
                if (manblood>-2&&behit1==behit2)
                {
                    manblood=manblood-1;
                    playbehitmusic();
                    behit2=0;
                }


            }
            else
            {
                putimage(man_x,man_y,140,140,&img_right_mask,man_i*140,0,NOTSRCERASE);
                putimage(man_x,man_y,140,140,&img_right,man_i*140,0,SRCINVERT);
            
            }
            if (showray)
            {
                putimage(man_x+120,man_y+40,1200,60,&img_right_ray_mask,0,0,NOTSRCERASE);
                putimage(man_x+120,man_y+40,1200,60,&img_right_ray,0,0,SRCINVERT);
                raytime2=clock();
            }
            
        }
        if(left )
        {
            
            
            if(is_hit1)
            {
                putimage(man_x,man_y,140,140,&img_left_hit1_mask,0,0,NOTSRCERASE);
                putimage(man_x,man_y,140,140,&img_left_hit1,0,0,SRCINVERT);

                is_playhit1music=1;

                have_hit=1;
                
            }
            
            else if(is_hit2)
            {
                putimage(man_x,man_y,140,140,&img_left_hit2_mask,0,0,NOTSRCERASE);
                putimage(man_x,man_y,140,140,&img_left_hit2,0,0,SRCINVERT);

                is_playhit2music=1;

                have_hit=1;
            }
            else
            {
                putimage(man_x,man_y,140,140,&img_left_mask,man_i*140,0,NOTSRCERASE);
                putimage(man_x,man_y,140,140,&img_left,man_i*140,0,SRCINVERT);
                
            }
            if (showray)
            {
                putimage(man_x-1180,man_y+40,1200,60,&img_left_ray_mask,0,0,NOTSRCERASE);
                putimage(man_x-1180,man_y+40,1200,60,&img_left_ray,0,0,SRCINVERT);
                raytime2=clock();
            }
            
        }
    //阳光框和数字
            putimage(0,0,78,87,&img_sunwindow_mask,0,0,NOTSRCERASE);
            putimage(0,0,78,87,&img_sunwindow,0,0,SRCINVERT);

            if (sunNum==0)
            {    
                putimage(30,62,14,20,&img_0_mask,0,0,NOTSRCERASE);
                putimage(30,62,14,20,&img_0,0,0,SRCINVERT);
            }
            if (sunNum==10)
            {
                putimage(25,62,14,20,&img_1_mask,0,0,NOTSRCERASE);
                putimage(25,62,14,20,&img_1,0,0,SRCINVERT);
                
                putimage(40,62,14,20,&img_0_mask,0,0,NOTSRCERASE);
                putimage(40,62,14,20,&img_0,0,0,SRCINVERT);
            }
            if (sunNum==20)
            {
                putimage(20,62,14,20,&img_2_mask,0,0,NOTSRCERASE);
                putimage(20,62,14,20,&img_2,0,0,SRCINVERT);
                
                putimage(40,62,14,20,&img_0_mask,0,0,NOTSRCERASE);
                putimage(40,62,14,20,&img_0,0,0,SRCINVERT);
            }
        
            if (sunNum==30)
            {
                putimage(20,62,14,20,&img_3_mask,0,0,NOTSRCERASE);
                putimage(20,62,14,20,&img_3,0,0,SRCINVERT);
                
                putimage(40,62,14,20,&img_0_mask,0,0,NOTSRCERASE);
                putimage(40,62,14,20,&img_0,0,0,SRCINVERT);
            }
        
            if (sunNum==40)
            {
                putimage(20,62,14,20,&img_4_mask,0,0,NOTSRCERASE);
                putimage(20,62,14,20,&img_4,0,0,SRCINVERT);
                
                putimage(40,62,14,20,&img_0_mask,0,0,NOTSRCERASE);
                putimage(40,62,14,20,&img_0,0,0,SRCINVERT);
            }
        
            if (sunNum==50)
            {
                putimage(20,62,14,20,&img_5_mask,0,0,NOTSRCERASE);
                putimage(20,62,14,20,&img_5,0,0,SRCINVERT);
                
                putimage(40,62,14,20,&img_0_mask,0,0,NOTSRCERASE);
                putimage(40,62,14,20,&img_0,0,0,SRCINVERT);
            }
        
            if (sunNum==60)
            {
                putimage(20,62,14,20,&img_6_mask,0,0,NOTSRCERASE);
                putimage(20,62,14,20,&img_6,0,0,SRCINVERT);
                
                putimage(40,62,14,20,&img_0_mask,0,0,NOTSRCERASE);
                putimage(40,62,14,20,&img_0,0,0,SRCINVERT);
            }
        
            if (sunNum==70)
            {
                putimage(20,62,14,20,&img_7_mask,0,0,NOTSRCERASE);
                putimage(20,62,14,20,&img_7,0,0,SRCINVERT);
                
                putimage(40,62,14,20,&img_0_mask,0,0,NOTSRCERASE);
                putimage(40,62,14,20,&img_0,0,0,SRCINVERT);
            }
        
            if (sunNum==80)
            {
                putimage(20,62,14,20,&img_8_mask,0,0,NOTSRCERASE);
                putimage(20,62,14,20,&img_8,0,0,SRCINVERT);
                
                putimage(40,62,14,20,&img_0_mask,0,0,NOTSRCERASE);
                putimage(40,62,14,20,&img_0,0,0,SRCINVERT);
            }
        
            if (sunNum==90)
            {
                putimage(20,62,14,20,&img_9_mask,0,0,NOTSRCERASE);
                putimage(20,62,14,20,&img_9,0,0,SRCINVERT);
                
                putimage(40,62,14,20,&img_0_mask,0,0,NOTSRCERASE);
                putimage(40,62,14,20,&img_0,0,0,SRCINVERT);
            }
        
            if (sunNum==100)
            {
                putimage(15,62,7,20,&img_1_mask,0,0,NOTSRCERASE);
                putimage(15,62,7,20,&img_1,0,0,SRCINVERT);

                putimage(27,62,14,20,&img_0_mask,0,0,NOTSRCERASE);
                putimage(27,62,14,20,&img_0,0,0,SRCINVERT);

                putimage(46,62,14,20,&img_0_mask,0,0,NOTSRCERASE);
                putimage(46,62,14,20,&img_0,0,0,SRCINVERT);
            }
        
        


            //奥特曼血条
            if (manblood==20)
            {
                putimage(120,-35,128,128,&img_manblood10_mask,0,0,NOTSRCERASE);
                putimage(120,-35,128,128,&img_manblood10,0,0,SRCINVERT);
            }
            if (manblood==18||manblood==19)
            {
                putimage(120,-35,128,128,&img_manblood9_mask,0,0,NOTSRCERASE);
                putimage(120,-35,128,128,&img_manblood9,0,0,SRCINVERT);
            }
            if (manblood==16||manblood==17)
            {
                putimage(120,-35,128,128,&img_manblood8_mask,0,0,NOTSRCERASE);
                putimage(120,-35,128,128,&img_manblood8,0,0,SRCINVERT);
            }
            if (manblood==14||manblood==15)
            {
                putimage(120,-35,128,128,&img_manblood7_mask,0,0,NOTSRCERASE);
                putimage(120,-35,128,128,&img_manblood7,0,0,SRCINVERT);
            }
            if (manblood==12||manblood==13)
            {
                putimage(120,-35,128,128,&img_manblood6_mask,0,0,NOTSRCERASE);
                putimage(120,-35,128,128,&img_manblood6,0,0,SRCINVERT);
            }
            if (manblood==10||manblood==11)
            {
                putimage(120,-35,128,128,&img_manblood5_mask,0,0,NOTSRCERASE);
                putimage(120,-35,128,128,&img_manblood5,0,0,SRCINVERT);
            }
            if (manblood==8||manblood==9)
            {
                putimage(120,-35,128,128,&img_manblood4_mask,0,0,NOTSRCERASE);
                putimage(120,-35,128,128,&img_manblood4,0,0,SRCINVERT);
            }
            if (manblood==6||manblood==7)
            {
                putimage(120,-35,128,128,&img_manblood3_mask,0,0,NOTSRCERASE);
                putimage(120,-35,128,128,&img_manblood3,0,0,SRCINVERT);
            }
            if (manblood==4||manblood==5)
            {
                putimage(120,-35,128,128,&img_manblood2_mask,0,0,NOTSRCERASE);
                putimage(120,-35,128,128,&img_manblood2,0,0,SRCINVERT);
            }
            if (manblood==2||manblood==3)
            {
                putimage(120,-35,128,128,&img_manblood1_mask,0,0,NOTSRCERASE);
                putimage(120,-35,128,128,&img_manblood1,0,0,SRCINVERT);
            }
            if (manblood<=1)
            {
                putimage(120,-35,128,128,&img_manblood0_mask,0,0,NOTSRCERASE);
                putimage(120,-35,128,128,&img_manblood0,0,0,SRCINVERT);
            }
            

            //进度条
            if (nowtime-starttime>0&&nowtime-starttime<=8000)
            {    
                putimage(600,570,157,21,&img_ing0_mask,0,0,NOTSRCERASE);
                putimage(600,570,157,21,&img_ing0,0,0,SRCINVERT);
            }

            if (nowtime-starttime>8000&&nowtime-starttime<=36000)
            {    
                putimage(600,570,157,21,&img_ing1_mask,0,0,NOTSRCERASE);
                putimage(600,570,157,21,&img_ing1,0,0,SRCINVERT);
            }

            if (nowtime-starttime>36000&&nowtime-starttime<=74000)
            {    
                putimage(600,570,157,21,&img_ing2_mask,0,0,NOTSRCERASE);
                putimage(600,570,157,21,&img_ing2,0,0,SRCINVERT);
            }

            if (nowtime-starttime>74000&&nowtime-starttime<=110000)
            {    
                putimage(600,570,157,21,&img_ing3_mask,0,0,NOTSRCERASE);
                putimage(600,570,157,21,&img_ing3,0,0,SRCINVERT);
            }

            if (nowtime-starttime>110000&&nowtime-starttime<=150000)
            {    
                putimage(600,570,157,21,&img_ing4_mask,0,0,NOTSRCERASE);
                putimage(600,570,157,21,&img_ing4,0,0,SRCINVERT);
            }

            if (nowtime-starttime>150000&&nowtime-starttime<=185000)
            {    
                putimage(600,570,157,21,&img_ing5_mask,0,0,NOTSRCERASE);
                putimage(600,570,157,21,&img_ing5,0,0,SRCINVERT);
            }

            if (nowtime-starttime>185000&&nowtime-starttime<=230000)
            {    
                putimage(600,570,157,21,&img_ing6_mask,0,0,NOTSRCERASE);
                putimage(600,570,157,21,&img_ing6,0,0,SRCINVERT);
            }

            if (nowtime-starttime>230000&&nowtime-starttime<=280000)
            {    
                putimage(600,570,157,21,&img_ing7_mask,0,0,NOTSRCERASE);
                putimage(600,570,157,21,&img_ing7,0,0,SRCINVERT);
            }

            if (nowtime-starttime>280000&&nowtime-starttime<=320000)
            {    
                putimage(600,570,157,21,&img_ing8_mask,0,0,NOTSRCERASE);
                putimage(600,570,157,21,&img_ing8,0,0,SRCINVERT);
            }

            if (nowtime-starttime>320000&&nowtime-starttime<=360000)
            {    
                putimage(600,570,157,21,&img_ing9_mask,0,0,NOTSRCERASE);
                putimage(600,570,157,21,&img_ing9,0,0,SRCINVERT);
            }

            if (nowtime-starttime>360000)
            {    
                putimage(600,570,157,21,&img_ing10_mask,0,0,NOTSRCERASE);
                putimage(600,570,157,21,&img_ing10,0,0,SRCINVERT);
            }
            
            
           // 中间的进程
            putimage(640,585,86,11,&img_inging_mask,0,0,NOTSRCERASE);
            putimage(640,585,86,11,&img_inging,0,0,SRCINVERT);
             //僵尸头
            putimage(zombiehead_x,570,22,23,&img_zombiehead_mask,0,0,NOTSRCERASE);
            putimage(zombiehead_x,570,22,23,&img_zombiehead,0,0,SRCINVERT);

            for (i=0;i<30;i++)
            {
                if (show_sun[i])
                {
                    putimage(sun_x[i],sun_y[i],50,50,&img_sun_mask,0,0,NOTSRCERASE);
                    putimage(sun_x[i],sun_y[i],50,50,&img_sun,0,0,SRCINVERT);
                }
            }
            if (showprepare)
            {
                if (had_playprepare==0)
                {
                    mciSendString("open .\\prepare.mp3 alias preparemusic",NULL,0,NULL);//背景音乐
                    mciSendString("play preparemusic",NULL,0,NULL);//循环播放
                    had_playprepare=1;
                }
                putimage(200,300,300,127,&img_prepare_mask,0,0,NOTSRCERASE);
                putimage(200,300,300,127,&img_prepare,0,0,SRCINVERT);
            }
            if (showlast)
            {
                if (had_playlast==0)
                {
                    mciSendString("open .\\last.mp3 alias lastmusic",NULL,0,NULL);//背景音乐
                    mciSendString("play lastmusic",NULL,0,NULL);//循环播放
                    had_playlast=1;
                }
                putimage(500,500,286,34,&img_coming_mask,0,0,NOTSRCERASE);
                putimage(500,500,286,34,&img_coming,0,0,SRCINVERT);
            }

            if (win)
            {
                
                //关闭所有音乐，画面暂停
                putimage(330,220,166,134,&img_win_mask,0,0,NOTSRCERASE);
                putimage(330,220,166,134,&img_win,0,0,SRCINVERT);

                mciSendString("close gamemusic", NULL, 0, NULL); //关闭游戏音乐
                mciSendString("open .\\win.mp3 alias winmusic",NULL,0,NULL);//背景音乐
                mciSendString("play winmusic",NULL,0,NULL);//循环播放
                
                stop=1;
            }
            if (failure)
            {
                
                //关闭所有音乐，画面暂停
                putimage(118,66,564,468,&img_failure_mask,0,0,NOTSRCERASE);
                putimage(118,66,564,468,&img_failure,0,0,SRCINVERT);
                
                mciSendString("close gamemusic", NULL, 0, NULL); //关闭游戏音乐
                mciSendString("open .\\failure.mp3 alias failuremusic",NULL,0,NULL);//背景音乐
                mciSendString("play failuremusic",NULL,0,NULL);//循环播放
                
                stop=1;
                
            }

            

    }

    // 延时
    FlushBatchDraw();
    Sleep(1);
}

void updateWithoutInput()
{

    if(is_playstart&&had_playstart==0)
    {
        playstartmusic();   //播放开始音乐
        had_playgame=0;
        had_playstart=1;
        had_playhelp=0;
    }
    if(is_playhelp&&had_playhelp==0)
    {
        playhelpmusic();   //播放帮助音乐
        had_playgame=0;
        had_playstart=0;
        had_playhelp=1;
    }
    if(is_playgame&&had_playgame==0)
    {    
        playgamemusic();   //播放开始音乐
        had_playgame=1;
        had_playstart=0;
        had_playhelp=0;
    }


    nowtime=clock();


//在游戏游戏界面时的
    if(show_game)
    {

        if (nowtime-starttime<5000)
        {
            bk_i=bk_i+bk_j;
        if (bk_i<=-400)
            bk_j=1;
        if(bk_j==1&&bk_i==-120)
            bk_j=0;
        }

        if (stop==0)
        {

            if (raytime2-raytime1>1500)
            {
                showray=0;
            }
            zombiehead_x=732-(nowtime-starttime)*11/30000;
            
            for (i=0;i<30;i++)
            {
                if (  nowtime -starttime<=12050*(i+1) && nowtime -starttime>=12000*(i+1)  )
                {
                    show_sun[i]=1;
                    
                }
                if (  nowtime -starttime<=22100*(i+1) && nowtime -starttime>=21800*(i+1)  )
                {
                    show_sun[i]=0;
                    
                }
            }
        
            //大僵尸的移动速度和步频
            
            for (i=0;i<5;i++)
            {
                for (j=0;j<40;j++)
                {
                    if (is_show_zombie[i][j]>4&&is_zombie_dead[i][j]==0)
                    {
                        
                        if ((man_x-zombie_x[i][j]>-120&&man_x-zombie_x[i][j]<0)&&(abs(man_y-zombie_y[i][j])<50) )
                        {
                            if (hadtime1[i][j]==0)
                            {
                                nowtime1[i][j]=clock();
                                hadtime1[i][j]=1;
                            }
                            
                            
                            zombie_stop[i][j]=1;//僵尸停止移动
                            
                            if (zombie_type[i][j]==1)
                            {    
                                zombie_j[i][j]=((nowtime-starttime)/150)%14;
                            }
                            if (zombie_type[i][j]==2)
                            {
                                zombie_j[i][j]=((nowtime-starttime)/150)%9;
                            }
                            if (zombie_type[i][j]==3)
                            {
                                zombie_j[i][j]=((nowtime-starttime)/150)%16;
                            }
                            if (zombie_type[i][j]==4)
                            {
                                zombie_j[i][j]=((nowtime-starttime)/150)%9;    
                            }
                            
                            
                            
                        }
                        else
                        {
                            if (hadtime2[i][j]==0&&hadtime1[i][j]==1)
                            {
                                nowtime2[i][j]=clock();
                                hadtime2[i][j]=1;    
                            }
                            
                            
                            zombie_stop[i][j]=0;//僵尸恢复移动
                            
                            if (zombie_type[i][j]==1)
                            {
                                zombie_x[i][j]=zombie_startx[i][j]-(nowtime-starttime-nowtime2[i][j]+nowtime1[i][j])/80;
                                zombie_j[i][j]=((nowtime-starttime)/150)%14;
                            }
                            
                            if (zombie_type[i][j]==2)
                            {
                                zombie_x[i][j]=zombie_startx[i][j]-(nowtime-starttime-nowtime2[i][j]+nowtime1[i][j])/80;
                                zombie_j[i][j]=((nowtime-starttime)/150)%9;
                            }
                            if (zombie_type[i][j]==3)
                            {
                                zombie_x[i][j]=zombie_startx[i][j]-(nowtime-starttime-nowtime2[i][j]+nowtime1[i][j])/80;
                                zombie_j[i][j]=((nowtime-starttime)/150)%16;
                            }
                            if (zombie_type[i][j]==4)
                            {
                                
                                zombie_x[i][j]=zombie_startx[i][j]-(nowtime-starttime-nowtime2[i][j]+nowtime1[i][j])/80;
                                zombie_j[i][j]=((nowtime-starttime)/150)%9;
                                
                            }
                            
                        }
                        
                        if (man_hit==0&&zombie_stop[i][j]==1)
                        {
                            be_hit=1;
                        }
                        
                        
                        //判断僵尸是否死亡
                        if (zombieblood[i][j]<=0)
                            is_zombie_dead[i][j]=1;
                        
                        //判断奥特曼是否死亡
                        if (manblood<=0)
                            is_man_dead=1;
                    }
                    
                }
                
            }

            //是否播放准备画面
            if (nowtime-starttime>4000&&nowtime-starttime<8000)
            {
                showprepare=1;
            }
            else
                showprepare=0;
            //是否播放最后一波画面
            if (nowtime-starttime>312000&&nowtime-starttime<315000)
            {
                showlast=1;
            }
            else
            showlast=0;
        }


        
    

        //失败的条件
        for (i=0;i<5;i++)
        {
            for (j=0;j<40;j++)
            {
                if (is_zombie_dead[i][j]==0)
                {
                    if (zombie_x[i][j]<0)
                    {
                        failure=1;
                    }
                }
            }
        }
        if (manblood<0)
        {
            failure=1;
        }
        //胜利的条件
        win_i=0;
        if (nowtime-starttime>350000)
        {
            for (i=0;i<5;i++)
            {
                for (j=35;j<40;j++)
                {
                    if (is_zombie_dead[i][j]==1)
                    {
                        win_i++;
                    }
                }
            }
            
        }
        if (win_i>=25&&failure==0)
        {
            win=1;
        }





        }


}

void updateWithInput()
{
    MOUSEMSG m;//记录鼠标消息
    while (MouseHit())
    {
        m=GetMouseMsg();
        
        if (m.uMsg==WM_LBUTTONDOWN)        //当鼠标按下时
            
        {
        //在游戏开始界面时
            if(show_start)
            {
                //鼠标在开始游戏位置时按下鼠标左键时进入游戏
                if((m.x<520&&m.x>400&&m.y>288&&m.y<360)
                    ||(m.x<680&&m.x>520&&m.y>264&&m.y<336))
                {
                    show_game=1;
                    show_start=0;
                    show_help=0;
                    starttime=clock();        
                    mciSendString("open .\\start1.mp3 alias start1music",NULL,0,NULL);
                    mciSendString("play start1music ",NULL,0,NULL);
                    
                    
                }
                
                //鼠标在游戏帮助位置时按下鼠标左键时进入游戏帮助页面
                else if ( (m.x<520&&m.x>400&&m.y>336&&m.y<408)
                    ||(m.x<600&&m.x>520&&m.y>360&&m.y<432)
                    ||(m.x<680&&m.x>600&&m.y>384&&m.y<432)  )
                {
                    show_game=0;
                    show_start=0;
                    show_help=1;
                }
            }
        //在游戏帮助界面时
            else if(show_help)
            {    
                //鼠标在主菜单位置时按下鼠标左键时进入主菜单页面
                if ( (m.x<580&&m.x>240&&m.y>468&&m.y<552)  )
                {
                    show_game=0;
                    show_start=1;
                    show_help=0;
                }

            }

            //在游戏画面时
            else if(show_game)
            {    
                //鼠标在阳光内
                for (i=0;i<30;i++)
                {

                    if (show_sun[i])
                    {

                        if (  (m.x<(sun_x[i]+50))  && (m.x>sun_x[i]) && (m.y>sun_y[i]) && (m.y<(sun_y[i]+50))  )
                        {
                            
                            show_sun[i]=0;

                            mciSendString("close sunmusic",NULL,0,NULL);//关闭先前一次的音乐
                            mciSendString("open .\\sun.mp3 alias sunmusic",NULL,0,NULL);
                            mciSendString("play sunmusic ",NULL,0,NULL);

                            if (sunNum< 100)
                            {
                                sunNum=sunNum+10;
                            }
                            if (manblood< 19)
                            {
                                manblood=manblood+2;
                            }
                            
                            
                        }
                    }
                
                }    
            }
         
        }
    
    }



    if (nowtime-starttime>5000)
    {
        char input;
    if(kbhit())  // 判断是否有输入
    {
        input = getch();  // 根据用户的不同输入来移动，不必输入回车
        if (input == 'a')   
        {    
            clearrectangle(man_x,man_y,man_x+140,man_y+140);
            man_i++;
            if (man_x>=0)
            {
                man_x=man_x-15;    
            }
        
            if(man_i==4)
                man_i=0;
            right=0;
            left=1;
            is_hit1=0;
            is_hit2=0;
            mciSendString("close runmusic",NULL,0,NULL);//关闭先前一次的音乐
            mciSendString("open .\\run.mp3 alias runmusic",NULL,0,NULL);
            mciSendString("play runmusic ",NULL,0,NULL);

                man_hit=0;
                be_hit=0;
            // 位置左移
            
        }
        if (input == 'd')
        {
            clearrectangle(man_x,man_y,man_x+140,man_y+140);
            man_i++;
            if (man_x<660)
            {
                man_x=man_x+15;
            }
        

            if(man_i==4)
                man_i=0;
            right=1;
            left=0;
            is_hit1=0;
            is_hit2=0;
            
            mciSendString("close runmusic",NULL,0,NULL);//关闭先前一次的音乐
            mciSendString("open .\\run.mp3 alias runmusic",NULL,0,NULL);
            mciSendString("play runmusic ",NULL,0,NULL);

                man_hit=0;
                be_hit=0;
            // 位置右移
            
        }
        if (input == 'w')
        {    
            clearrectangle(man_x,man_y,man_x+140,man_y+140);
            man_i++;

            if (man_y>50)
            {
                man_y=man_y-8;
            }
            
            if(man_i==4)
                man_i=0;
            is_hit1=0;
            is_hit2=0;
            mciSendString("close runmusic",NULL,0,NULL);//关闭先前一次的音乐
            mciSendString("open .\\run.mp3 alias runmusic",NULL,0,NULL);
            mciSendString("play runmusic ",NULL,0,NULL);

                man_hit=0;
                be_hit=0;
            // 位置上移
            
        }

        if (input == 's')
        {
            clearrectangle(man_x,man_y,man_x+140,man_y+140);
            man_i++;
            if (man_y<410)
            {
                man_y=man_y+8;
            }
        
            if(man_i==4)
                man_i=0;
            is_hit1=0;
            is_hit2=0;
            mciSendString("close runmusic",NULL,0,NULL);//关闭先前一次的音乐
            mciSendString("open .\\run.mp3 alias runmusic",NULL,0,NULL);
            mciSendString("play runmusic ",NULL,0,NULL);

                man_hit=0;
                be_hit=0;
            // 位置下移
            
        }
        if (input == 'j')
        {
            mciSendString("close hit1music",NULL,0,NULL);//关闭先前一次的音乐
            mciSendString("open .\\hit1.wav alias hit1music",NULL,0,NULL);
            mciSendString("play hit1music ",NULL,0,NULL);

            for (i=0;i<5;i++)
            {
                for (j=0;j<40;j++)
                {
                    if((abs(man_x-zombie_x[i][j])<120)&&(abs(man_y-zombie_y[i][j])<50) )
                    {
                        zombieblood[i][j]--;
                    }
                }
            }

            clearrectangle(man_x,man_y,man_x+140,man_y+140);
            if(have_hit)
            {    
                is_hit1=0;
                have_hit=0;
                
            }
            else
                is_hit1=1;

            man_hit=1;
            be_hit=0;

        }
        if (input == 'k')
        {
            if (sunNum>0)
            {
                mciSendString("close raymusic",NULL,0,NULL);//关闭先前一次的音乐
                mciSendString("open .\\ray.mp3 alias raymusic",NULL,0,NULL);
                mciSendString("play raymusic ",NULL,0,NULL);
                
                for (i=0;i<5;i++)
                {
                    for (j=0;j<40;j++)
                    {
                        if (right)
                        {
                            if (zombie_x[i][j]<780&&zombie_x[i][j]>man_x&&(abs(man_y-zombie_y[i][j])<50) )
                            {
                                is_zombie_dead[i][j]=1;
                            }
                        }
                        if (left)
                        {
                            if (zombie_x[i][j]<man_x&&(abs(man_y-zombie_y[i][j])<50) )
                            {
                                is_zombie_dead[i][j]=1;
                            }
                        }
                        
                    }
                }
                
                showray=1;
                raytime1=clock();
                sunNum=sunNum-10;
            }
        
        }


        if(input==27)
        {
            for(;;)
            {
                mciSendString("close helpmusic", NULL, 0, NULL); //关闭帮助音乐
                mciSendString("close startmusic", NULL, 0, NULL); //关闭开始音乐
                mciSendString("close gamemusic", NULL, 0, NULL); //关闭游戏音乐
                if(getch()==27)
                {
                
                    mciSendString("open .\\game.mp3 alias gamemusic",NULL,0,NULL);//背景音乐
                    mciSendString("play gamemusic repeat",NULL,0,NULL);//循环播放
                    break;
                }
            }
            
        }
        
        }
    }
    




}

void gameover()
{


    EndBatchDraw();
    getch();
    closegraph();
}

int main()
{

    startup();  // 数据初始化    
    while (1)  //  游戏循环执行
    {
        show();  // 显示画面
        updateWithoutInput();  // 与用户输入无关的更新
        updateWithInput();     // 与用户输入有关的更新
    }
    gameover();     // 游戏结束、后续处理
    return 0;
}